+++
title = "Instalar Arch Linux en Kimsufi"
date = 2023-11-19T16:52:40Z
+++

Si tenemos un servidor en [Kimsufi][1] alquilado desde hace tiempo nuestra
manera de acceder al panel de control del mismo no será por el nuevo panel
de OVH donde tenemos bastantes opciones, sino que utilizaremos el [panel
parco y _viejuno_ de Kimsufi][2] donde las opciones son escuetas. En este
panel, aunque podemos hacer reinstalaciones limpias del sistema, no aparece
la posibilidad de instalación de Arch Linux, pero que eso no te desanime ya
que podemos hacerlo de manera manual y que nos quede lo mas limpio posible.

## Preparación

Antes de empezar lo mas importante es hacer un _backup_ de aquello que no
quieras perder de lo actualmente tienes en el servidor. Si no tienes nada
y puedes borrarlo sin problemas simplemente acuérdate de copiar el fichero
de configuración de red que deberías tener en `/etc/systemd/network` ya que,
aunque se puede configurar el sistema para que coja IP por medio de DHCP, si
tienes una configuración hecha con asignación de IP fija es mejor.

## Reinicio en modo rescate

De las pocas opciones que nos da el panel una de ellas es _Netboot_ en la
que podemos indicar como queremos que la maquina arranque, si con su disco
duro (opción de funcionamiento por defecto), con arranque por red (que de
poco vale porque no tiene opciones) o con un sistema de rescate. Este
sistema de rescate es útil cuando nos quedamos sin conexión en la máquina
y queremos ver que es lo que ha pasado y, en este caso, para hacer la
instalación. Seleccionamos el arranque en modo rescate (el único modo
disponible es un `rescue-customer`, una Debian), pulsamos siguiente
y confirmamos. Esto solo cambia el modo de arranque pero no reinicia la
maquina, esto lo haremos nosotros o bien vía SSH o bien desde la propia
consola en el botón reiniciar. Tras esperar un rato (unos 5 minutos) nos
llegará un correo indicando como debemos conectarnos a este modo rescate.

## Reparticionado

Una vez que hayamos accedido vía SSH con las instrucciones que nos han
llegado veremos que si hacemos un `fdisk -l` tenemos acceso a nuestro disco
en `/dev/sda` y podemos reparticionarlo como queramos. En mi caso he hecho
3 particiones primarias, la primera para `/boot` que formatearé en `ext4`,
la segunda como `swap` y la tercera para `/` en `btrfs`.

## Formateando y montando

Como comenté anteriormente tengo 3 particiones.

- `/dev/sda1`, para `/boot` en `ext4`.
- `/dev/sda2`, para `swap`
- `/dev/sda3`, para `/` en `btrfs`.

Por lo que los comandos a ejecutar para formatearlas son.
```sh
mkfs.ext4 -L boot /dev/sda1
mkswap -L swap /dev/sda2
mkfs.btrfs -L root /dev/sda3
```

A mayores en mi partición `btrfs` quiero tener unos subvolúmenes para el
sistema, el `/home`, la caché de pacman `/var/cache/pacman/pkg` y los
contenedores `/var/lib/containers`. Para ello primero montamos `/dev/sda3`
y luego creamos los subvolúmenes.
```sh
mount /dev/sda3 /mnt
btrfs subvolume create /mnt/@
btrfs subvolume create /mnt/@home
btrfs subvolume create /mnt/@pacman
btrfs subvolume create /mnt/@containers
umount /mnt
```

Y ahora ya podemos montar todo.
```sh
mount -o subvol=@,compress=lzo /dev/sda3 /mnt
mkdir -p /mnt/boot /mnt/home /mnt/var/cache/pacman/pkg /mnt/var/lib/containers
mount /dev/sda1 /mnt/boot
mount -o subvol=@home,compress=lzo /dev/sda3 /mnt/home
mount -o subvol=@pacman,compress=lzo /dev/sda3 /mnt/var/cache/pacman/pkg
mount -o subvol=@containers,compress=lzo /dev/sda3 /mnt/var/lib/containers
```

Como es evidente, este es un ejemplo de particionado y montaje, cada uno lo
debe de adecuar a sus necesidades, por ejemplo si no vas a usar contenedores
`podman` no tiene sentido que crees `@containers`. Al igual que si quieres
utilizar `XFS` pues no vas a tener subvolúmenes. Esto ya es elección tuya.

## Instalando

Ahora en una instalación normal tocaría hacer el `pacstrap`, como estamos en
un sistema de rescate basado en Debian ese comando no lo tenemos, pero por
suerte un proyecto llamado [arch-bootstrap][3] que viene al rescate. En
esencia es simplemente un script bash que hace ese trabajo. Para ejecutarlo
lo mas sencillo es bajarlo directamente en el `home` del `root` del sistema
de rescate, darle permisos de ejecución y lanzarlo. Hay que tener en cuenta
que tiene como dependencia `zstd` (para descomprimir los paquetes), comando
que por defecto no esta en el sistema de rescate, pero que podemos
fácilmente instalar con `apt`.
```sh
apt update
apt install zstd
wget https://raw.githubusercontent.com/tokland/arch-bootstrap/master/arch-bootstrap.sh
chmod 755 arch-bootstrap.sh
./arch-bootstrap.sh /mnt
```

Lo siguiente sería ejecutar `genfstab` para generar el fichero `fstab` pero
tampoco lo tenemos, como también es un script podemos bajar el paquete de
Arch, descomprimirlo en el `home` del `root` y ejecutarlo.
```sh
wget -O arch-install-scripts.tar.zst https://archlinux.org/packages/extra/any/arch-install-scripts/download/
tar xf arch-install-scripts.tar.zst
usr/bin/genfstab -t UUID /mnt >> /mnt/etc/fstab
```

Ahora ya podemos hacer el `chroot` para darle los toques finales a la
instalación. Con un medio instalador de Arch tendíamos el comando
`arch-chroot` pero como aquí no lo tenemos lo emulamos manualmente.
```sh
mount --bind /proc /mnt/proc
mount --bind /sys /mnt/sys
mount --bind /dev /mnt/dev
mount --bind /dev/pts /mnt/dev/pts
chroot /mnt bash
```

Dentro del `chroot` finalizamos la instalación. Aquí ya es un poco al gusto
de cada uno hasta donde quieras llegar, como mínimo se debería de poner
nombre a la maquina, configurarle la red, crear un usuario con posibilidad
de acceso `ssh` e instalar `grub` y el `kernel`, pero por poder se podría
incluso instalar todos los servicios que va a tener la máquina y sus
configuraciones. Yo os dejo aquí los comandos mínimos.
```sh
echo arch > /etc/hostname
ln -s /usr/share/zoneinfo/Europe/Paris /etc/localtime
sed -i 's/#en_US.UTF-8/en_US.UTF-8/g' /etc/locale.gen
locale-gen
echo LANG=en_US.UTF-8 > /etc/locale.conf
pacman -S --needed --noconfirm base-devel
sed -i 's/# %wheel ALL=(ALL:ALL) N/%wheel ALL=(ALL:ALL) N/g' /etc/sudoers
pacman -S --noconfirm btrfs-progs grub linux openssh
systemctl enable sshd systemd-networkd systemd-resolved
grub-install --target=i386-pc --recheck --debug /dev/sda
sed -i 's/GRUB_TIMEOUT=5/GRUB_TIMEOUT=1/g' /etc/default/grub
grub-mkconfig -o /boot/grub/grub.cfg
useradd -m usuario
echo usuario:contraseña | chpasswd
usermod -a -G log,adm,wheel,tty,disk usuario
```

Antes de salir del `chroot` acuérdate de restaurar el fichero de
configuración de la red en `/etc/systemd/network` para que cuando reinicies
la máquina tenga configuración de IP.

Otra de las cosas que se pueden hacer es, en lugar de establecerle una
contraseña al usuario, colocar tus llaves SSH en tu `home` en
`.ssh/authorized_keys` y así autenticar con llaves en lugar de contraseña.
```sh
mkdir -p /home/usuario/.ssh
echo 'llave ssh' > /home/usuario/.ssh/authorized_keys
chown -R usuario:usuario /home/usuario/.ssh
chmod 700 /home/usuario/.ssh
chmod 600 /home/usuario/.ssh/authorized_keys
```

Una vez que hayas salido del `chroot` desmonta todo `/mnt` y haz un par de
`sync` por si las moscas.
```sh
umount -R mnt
sync
sync
```

## Reiniciando en el nuevo sistema

Ahora que ya tienes el sistema instalado solo queda reiniciar. Primero
recuerda ir a la consola de Kimsufi y cambiar el arranque del sistema de
rescate al disco duro (accediendo a _Netboot_, seleccionando la opción
_Disco duro_, _Siguiente_ y _Confirmar_) y luego simplemente ejecuta un
`shutdown -r now` para que la maquina reinicie.

Si has hecho todo de manera correcta en unos minutos tu nuevo sistema
debería de responderte correctamente por SSH, si no, siempre puedes volver
a arrancar en modo rescate para ver que es lo que te puede haber faltado.

Y por cierto, no te olvides de actualizar el `/etc/resolv.conf` para que sea
un enlace simbólico a `/run/systemd/resolve/resolv.conf`.
```sh
rm /etc/resolv.conf && ln -s /run/systemd/resolve/resolv.conf
```

[1]:https://www.kimsufi.com
[2]:https://www.kimsufi.com/fr/manager/?lang=fr_FR#/login
[3]:https://github.com/tokland/arch-bootstrap
