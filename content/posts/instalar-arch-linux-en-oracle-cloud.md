+++
title = "Instalar Arch Linux en Oracle Cloud (Free Tier)"
date = 2024-12-19T10:58:41Z
+++

[Oracle cloud][oci] nos ofrece una mas que generosa [capa gratuita][ocifree]
que nos permite ir comprendiendo como funcionan sus sistemas para así poder
hacer una buena evaluación de los mismos. Dentro de las ventajas del _always
free_ tenemos la posibilidad de trabajar con hasta cuatro instancias Ampere
(ARM64) con la condición de no superar las tres mil horas de OCPU (si lo
hacemos dichas instancias se apagarán). El único pero que le podemos
encontrar a todo esto es que, al levantar una instancia, solo nos permite
trabajar con Oracle Linux, AlmaLinux, Rocky Linux y Ubuntu, nada de nuestra
distribución favorita Arch Linux, pero vamos a ponerle remedio a eso.

[oci]: https://www.oracle.com/es/cloud/
[ocifree]: https://www.oracle.com/es/cloud/free/

## Preparación inicial

Lo primero que tenemos que hacer es crear una instancia con la propia
consola del cloud. Da igual cual de los operativos permitidos escojamos
aunque yo en mi caso he utilizado Ubuntu.

Una vez levantada la instancia nos conectamos a ella vía SSH y descargamos
el `netboot.xyz-arm64.efi` en `/boot/efi` lo cual nos brindará de un
arranque por red y en memoria de diversos sistemas operativos.
```sh
sudo -s
cd /boot/efi
wget https://boot.netboot.xyz/ipxe/netboot.xyz-arm64.efi
```

## Arranque con netboot.xyz

En la consola del cloud accedemos a la instancia y, si bajamos un poco,
veremos en _Resources_ la opción _Console connection_, al entrar nos
aparecerá un botón _Launch Cloud Shell connection_, lo pulsamos y se nos
cargará un _shell_ en donde podemos controlar nuestra máquina. Le ordenamos
a la instancia que reinicie (esto podemos hacerlo vía SSH) y cuando en el
_shell_ veamos que la maquina empieza a apagarse presionaremos la tecla
_escape_ del teclado repetidamente hasta que veamos que nos aparece el KVM
(un menú con opciones). Con los cursores nos movemos a la opción _Boot
Manager_, accedemos con _intro_, seleccionamos _EFI Internal Shell_ y se nos
cargará una linea de comandos EFI. Aquí escribimos `netboot.xyz-arm64.efi`
y pulsamos de nuevo _intro_ para que cargue este fichero _efi_.

Veremos que la máquina comienza un arranque por red con este nuevo sistema,
esperamos unos segundos y nos aparecerá un nuevo menú. Seleccionamos aquí la
opción _Linux Network Installs (arm64)_, dentro de esta _Alpine Linux_
y _Alpine Linux X.X_ (siendo X.X la versión disponible de Alpine). Una vez
arrancado el sistema nos autenticaremos como _root_ (sin contraseña).

Para que nos sea mas cómodo trabajar (el _shell_ web es _poco amigable_)
habilitaremos el acceso SSH. Para ello primero configuramos la red generando
un fichero `/etc/network/interfaces` y la reiniciamos.
```sh
echo -e 'auto eth0\niface eth0 inet dhcp' > /etc/network/interfaces
/etc/init.d/networking restart
```

Luego configuramos SSH con la orden `setup-sshd` donde le indicamos que el
servidor deseado es `openssh` (opción por defecto), en _allow root login_ le
decimos `yes` y opcionalmente (aunque muy recomendable) podemos configurarle
una llave pública SSH. Si hemos configurado esta llave ya podríamos
conectarnos, en caso contrario debemos primero establecer una contraseña
para _root_ con la orden `passwd`. En cualquiera de ambos casos ya
deberíamos poder acceder vía SSH a `root@ip-de-la-instancia`.

## Preparando el sistema

Antes de poder realizar la instalación necesitamos obtener ciertas
herramientas, para ello configuraremos los repositorios de Alpine con la
orden `setup-apkrepos`. Debemos habilitar el repositorio de _Community_ con
`e` y luego con `f` buscar el _mirror_ mas rápido.

Instalamos las herramientas necesarias. En mi caso particular voy a utilizar
_btrfs_ como sistema de archivos raíz (y por tanto `btrfs-progs`). Si en tu
caso prefieres utilizar otro sistema de archivos no olvides instalar sus
herramientas correspondientes (por ejemplo `e2fsprogs` para _ext4_).
```sh
apk update
apk add arch-install-scripts dosfstools btrfs-progs
```

## Reparticionado

Si hacemos un `fdisk -l` veremos que el disco de sistema esta disponible
como `/dev/sda`. En mi caso voy a hacerle dos particiones, una primera de
512M para EFI y una segunda para el resto en _btrfs_, pero este no es el
único esquema válido por lo que puedes hacer lo que mejor se adapte a tus
necesidades, lo único que debes tener en cuenta es que es obligatorio que
exista una partición para el sistema EFI.
```
fdisk /dev/sda
Presionar "g" (create a new empty GPT partition table)
Presionar "n", partition default 1, First sector default 2048, Last sector +512M (set esp/EFI partition 1, size 512M)
Presionar "t", y "1" (selecciona el tipo como EFI System)
Presionar "n", partition default 2 (First/Last sector default)
Presionar "w" (write table to disk and exit)
```

Nota: Me he encontrado el caso de que después de particionar no me aparecían
en `/dev` los dispositivos `sda1`, etc. incluso ejecutando `partprobe`. Si
esto te sucede repite el proceso en dos pasos. Primero ejecutar `fdisk`,
crear una tabla de particiones vacía, guardar y salir y segundo ejecutarlo
de nuevo y crear las particiones.

## Formateando y montando

Formateamos la partición EFI como _vfat_ y la de sistema como _btrfs_.
```sh
mkfs.vfat -n BOOT /dev/sda1
mkfs.btrfs -L root /dev/sda2
```

Como la partición de sistema es _btrfs_ podemos crear subvolúmenes. En mi
caso crearé uno para el sistama, otro para _home_, un tercero para la caché
de _pacman_ y por último uno para `/var/lib/containers`. Se podrían crear
otros subvolúmenes, pero eso lo dejo a tu elección. Montamos temporalmente
`/dev/sda2` en `/mnt` y los creamos.
```sh
mount /dev/sda2 /mnt
btrfs subvolume create /mnt/@
btrfs subvolume create /mnt/@home
btrfs subvolume create /mnt/@pacman
btrfs subvolume create /mnt/@containers
umount /mnt
```

Y ahora ya podemos montar todo el sistema.
```sh
mount -o subvol=@,compress=lzo /dev/sda2 /mnt
mkdir -p /mnt/boot /mnt/home /mnt/var/cache/pacman/pkg /mnt/var/lib/containers
mount /dev/sda1 /mnt/boot
mount -o subvol=@home,compress=lzo /dev/sda2 /mnt/home
mount -o subvol=@pacman,compress=lzo /dev/sda2 /mnt/var/cache/pacman/pkg
mount -o subvol=@containers,compress=lzo /dev/sda2 /mnt/var/lib/containers
```

# Instalando

En este punto en un sistema x86_64 tocaría hacer `pacstrap /mnt` para
instalar el sistema base. Pero como esta máquina es ARM el sistema base nos
lo descargamos como un `tar.gz` que luego descomprimimos en `/mnt`.
```sh
cd /tmp
wget http://os.archlinuxarm.org/os/ArchLinuxARM-aarch64-latest.tar.gz
bsdtar -xpf ArchLinuxARM-aarch64-latest.tar.gz -C /mnt
rm ArchLinuxARM-aarch64-latest.tar.gz && cd
```

Nota: lo de borrar el fichero `ArchLinuxARM-aarch64-latest.tar.gz` es
opcional, recuerda que todo lo que no se almacene en `/mnt` esta en memoria.

Generamos el fichero `fstab` con los montajes de `/mnt` (la orden `-U` es
para que utilice los UUID de las particiones en lugar de `/dev/sdX`).
```sh
genfstab -U /mnt >> /mnt/etc/fstab
```

Ahora podríamos hacer el `arch-chroot`, pero antes aconsejo editar el
fichero `/mnt/etc/pacman.d/mirrorlist` para habilitar el mirror
`eu.mirror.archlinuxarm.org`. Esto se puede hacer también de esta forma.
```sh
mv /mnt/etc/pacman.d/mirrorlist /mnt/etc/pacman.d/mirrorlist.bak
echo 'Server = http://eu.mirror.archlinuxarm.org/$arch/$repo' > /mnt/etc/pacman.d/mirrorlist
echo 'Server = http://mirror.archlinuxarm.org/$arch/$repo' >> /mnt/etc/pacman.d/mirrorlist
```

Hacemos `arch-chroot /mnt` para terminar la instalación. Dentro del _chroot_
se pueden hacer muchas cosas, hay quien aprovecha e instala ya todo lo que
necesita, mi consejo es hacer lo mínimo para que la maquina arranque.
```sh
pacman-key --init
pacman-key --populate archlinuxarm
pacman -Syu --needed --noconfirm base-devel

echo arch > /etc/hostname
ln -s /usr/share/zoneinfo/GMT /etc/localtime
sed -i 's/#en_US.UTF-8/en_US.UTF-8/g' /etc/locale.gen
locale-gen
echo LANG=en_US.UTF-8 > /etc/locale.conf
echo KEYMAP=es > /etc/vconsole.conf
sed -i 's/# %wheel ALL=(ALL:ALL) N/%wheel ALL=(ALL:ALL) N/g' /etc/sudoers
pacman -S --noconfirm btrfs-progs openssh
systemctl enable sshd systemd-networkd systemd-resolved systemd-timesyncd
```

Por defecto en el `tar.gz` del sistema base hay creado un usuario _alarm_
con la misma contraseña. Si bien podríamos borrar este usuario y crear uno
nosotros (o simplemente utilizarlo), lo que vamos a hacer es renombrarlo
y cambiarle la contraseña (cambia `usuario` por el nombre que quieras).
```sh
sed -i 's/alarm/usuario/g' /etc/passwd /etc/shadow /etc/group /etc/gshadow /etc/subuid /etc/subgid
mv /home/alarm /home/usuario
usermod -a -G log,adm,wheel,tty,disk usuario
passwd usuario
```

Puedes aprovechar también para introducir tu llave SSH en el usuario.
```sh
mkdir /home/usuario/.ssh
echo 'llavessh' > /home/usuario/.ssh/authorized_keys
chown -R usuario:usuario /home/usuario/.ssh
chmod 700 /home/usuario/.ssh
chmod 600 /home/usuario/.ssh/authorized_keys
```

Además recuerda que por defecto en esta imagen del sistema el usuario _root_
tiene la misma contraseña (_root_) por lo que no olvides ejecutar `passwd`
para cambiarla por algo mas seguro.

Ahora lo único que falta es que la máquina arranque, para ello vamos
a utilizar `systemd-boot` que es lo mas cómodo y no requiere de instalar
nada a mayores ya que forma parte de _systemd_.
```sh
bootctl install
```

Esto nos deja el sistema de arranque instalado, pero nos falta crear un
fichero de configuración `arch.conf` en  `/boot/loader/entries` que le
indique que es lo que debe arrancar. El fichero debe contener lo siguiente.
```
title   Arch Linux
linux   /Image
initrd  /initramfs-linux.img
options root=PARTUUID=PART-UUID-DEL-ROOT rootfstype=btrfs rootflags=subvol=@ console=tty0 console=ttyS0,115200 rw
```

Como vemos en la linea de _options_ hay que especificar el PARTUUID de la
partición de sistema, para hacerlo lo mas sencillo es utilizar `blkid` para
obtener el valor, enviar esto al fichero y luego editarlo para incluir el
resto de opciones.
```sh
pacman -S --noconfirm vim # U otro editor que consideres
blkid -s PARTUUID /dev/sda2 > /boot/loader/entries/arch.conf
vim /boot/loader/entries/arch.conf # Y ya tenemos escrito el PARTUUID en el fichero
```

Ten en cuenta también que lo de `rootfstype=btrfs` y `rootflags=subvol=@` no
es necesario si tu sistema no esta formateado en _btrfs_.

Ahora ya podemos salir del _chroot_, desmontarlo todo, hacer un par de
_sync_ por si las moscas y reiniciar.
```sh
exit
umount -R mnt
sync
sync
reboot
```

## Arrancando el nuevo sistema

Si todo ha funcionado correctamente veremos en _shell_ del cloud como la
máquina arranca con Arch Linux. Podrás conectarte vía SSH con el usuario que
has creado o modificado.

Recuerda que si algo no te funcionase en el propio _shell_ se puede ver todo
el log de arranque e incluso (si este finaliza) autenticarte con tu usuario
(o como _root_) y hacer un diagnóstico del sistema.
