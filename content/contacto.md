+++
title = "Contacto"
extra.order = 4
+++

```rust
// Puedes ejecutar este código en https://play.rust-lang.org/
fn main() {
    const ADDR: &str = "contact";
    const NAME: &str = "ogarcia";
    const CHAT: &str = "moire";
    println!("mail: {}@{}.me", ADDR, NAME);
    println!("chat: @{}:{}.org", NAME, CHAT);
}
```

PGP Key: [0xd7d615b69dfc688e06120164bd41447de05755b9][pgp]

[pgp]: /ogarcia-pgp-key-pub.txt
