+++
title = "Proyectos"
extra.order = 3
+++

Puedes ver  mis proyectos en [GitLab][gitlab] y [GitHub][github]. En
[sourcehut][sourcehut] tengo un mirror de los mas importantes.

{% project(title="lesma",url="https://gitlab.com/ogarcia/lesma") %}
Aplicación sencilla de [_pastes_](https://es.wikipedia.org/wiki/Pastebin)
compatible con navegador y línea de comandos.
{% end %}

{% project(title="lesspass-client",url="https://gitlab.com/ogarcia/lesspass-client") %}
Biblioteca Rust y CLI cliente del servidor API de LessPass.
{% end %}

{% project(title="rlpcli",url="https://gitlab.com/ogarcia/rlpcli") %}
Pequeño CLI cliente del servidor API de LessPass para obtener contraseñas
y configuraciones del sitio.
{% end %}

{% project(title="Rockpass",url="https://gitlab.com/ogarcia/rockpass") %}
Servidor de API de LessPass.
{% end %}

{% project(title="TrelloWarrior",url="https://github.com/ogarcia/trellowarrior") %}
Herramienta para sincronizar proyectos de Taskwarrior con tableros de
Trello.
{% end %}

[gitlab]: https://gitlab.com/ogarcia
[github]: https://github.com/ogarcia
[sourcehut]: https://git.sr.ht/~ogarcia
